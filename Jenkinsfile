pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                checkout scmGit(branches:
                 [[name: '*/main']],
                 extensions: [],
                 userRemoteConfigs: [[credentialsId: 'jenkins-gitlab',
                 url: 'git@gitlab.com:eminehammi/tp-7-jenkins-ansible-docker.git']])
            }
        }

        stage('Build') {
            steps {
                sh 'mvn clean package'
                sh 'echo "Workspace Directory: ${workspace}"'
            }
        }

        stage('Create Docker image') {
            steps {
                sh "docker build -t aminehammi/image:${env.BUILD_NUMBER} ."
            }
        }

        stage('Push to Docker Hub') {
            steps {
                withCredentials([string(credentialsId: 'docker-hub-credentials', variable: 'DOCKER_HUB_TOKEN')]) {
                    script {
                        def dockerHubToken = env.DOCKER_HUB_TOKEN
                        sh """
                            docker login --username=aminehammi --password="${dockerHubToken}"
                            docker push aminehammi/image:${env.BUILD_NUMBER}
                        """
                    }
                }
            }
        }

        stage('Run Ansible Playbook: Install Docker') {
            steps {
                ansiblePlaybook becomeUser: 'jenkins',
                credentialsId: 'jenkins-user_jenkins',
                disableHostKeyChecking: true,
                installation: 'ansible',
                inventory: 'inventory.yml',
                playbook: 'install-docker-in-sv2.yml'
            }
        }
        
        stage('Run Ansible Playbook: Docker Image') {
            steps {
                ansiblePlaybook becomeUser: 'jenkins',
                credentialsId: 'jenkins-user_jenkins',
                disableHostKeyChecking: true,
                installation: 'ansible',
                inventory: 'inventory.yml',
                playbook: 'dockerimage.yml'
            }
        }
    }
}
